import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;

public class sort {

    public static void main(String[] args) throws IOException {
        String content = new String(Files.readAllBytes(Paths.get("src\\main\\resources\\file.txt")));
        System.out.println("Данные из файла: " + content);

        String[] nums = content.split(",");
        Integer[] ints = new Integer[nums.length];
        for (int i = 0; i < nums.length; i++) {
            ints[i] = Integer.valueOf(nums[i]);
        }

        Arrays.sort(ints);
        System.out.println("Сортировка по возрастанию: " + Arrays.toString(ints));


        Arrays.sort(ints, Collections.reverseOrder());
        System.out.println("Сортировка по убыванию: " + Arrays.toString(ints));
    }
}
