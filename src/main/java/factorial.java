import java.math.BigInteger;

public class factorial {

    public static void main(String[] args){
        BigInteger fact = BigInteger.valueOf(1);
        for (int i = 1; i <= 20; i++)
            fact = fact.multiply(BigInteger.valueOf(i));
        System.out.println("20! = " + fact);
    }
}
